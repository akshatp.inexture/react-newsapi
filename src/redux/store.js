import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import articleReducer from "./article/articleReducer";
import thunk from "redux-thunk";

const middleware = applyMiddleware(thunk);

const store = createStore(articleReducer, composeWithDevTools(middleware));

export default store;
