import { GET_DATA, Load_MORE, BOOK_MARK, REMOVE_BOOKMARK } from "./articletype";

const initialState = {
  news: [],
  search: "latest",
  load: 20,
  bookMark: JSON.parse(localStorage.getItem("Bookmark") || "[]"),
};

const articleReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA:
      return {
        ...state,
        news: action.payload.news,
        search: action.payload.search,
      };
    case Load_MORE:
      return {
        ...state,
        load: state.load + 20,
      };
    case BOOK_MARK:
      localStorage.setItem(
        "Bookmark",
        JSON.stringify([...state.bookMark, action.payload])
      );
      return {
        ...state,
        bookMark: [...state.bookMark, action.payload],
      };

    case REMOVE_BOOKMARK:
      const updatedBookmarks = state.bookMark.filter(
        (item) => item.title !== action.payload
      );
      localStorage.setItem("Bookmark", JSON.stringify(updatedBookmarks));
      return {
        ...state,
        bookMark: updatedBookmarks,
      };
    default:
      return state;
  }
};

export default articleReducer;
