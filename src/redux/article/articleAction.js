import { GET_DATA, Load_MORE, BOOK_MARK, REMOVE_BOOKMARK } from "./articletype";
import axios from "axios";

export const getData = (search) => async (dispatch, getState) => {
  console.log(search);
  let res = await axios.get(
    `http://newsapi.org/v2/everything?q=${search}&pageSize=100&sortBy=publishedAt&apiKey=1ff5b37a8cbc4886ad8bc7bfdb0fc7af`
  );
  // console.log(res.data.articles);

  const data = { search, news: res.data.articles };
  dispatch({
    type: GET_DATA,
    payload: data,
  });
};

export const loadMore = () => {
  return {
    type: Load_MORE,
  };
};

export const bookmark = (state) => {
  console.log(state);

  return {
    type: BOOK_MARK,
    payload: state,
  };
};

export const removeBookmark = (title) => {
  return {
    type: REMOVE_BOOKMARK,
    payload: title,
  };
};
