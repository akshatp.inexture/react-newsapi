import React, { useState, useEffect } from "react";
import {
  Nav,
  Navbar,
  Form,
  Container,
  FormControl,
  Button,
} from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

import { useDispatch, useSelector } from "react-redux";
import { getData } from "../redux/article/articleAction";

const Header = () => {
  const search = useSelector((state) => state.search);
  const [input, setInput] = useState(search);

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    // console.log(input);
    dispatch(getData(input));
  };

  console.log(search);

  useEffect(() => {
    dispatch(getData(input));
    //eslint-disable-next-line
  }, []);

  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Container fluid>
          <Navbar.Brand href="#">News Api</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <LinkContainer to="/">
                <Nav.Link>Home</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/bookmark">
                <Nav.Link>BookMark</Nav.Link>
              </LinkContainer>
            </Nav>
            <Form className="d-flex" onSubmit={handleSubmit}>
              <FormControl
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
                onChange={(e) => setInput(e.target.value)}
                value={input}
              />
              <Button variant="outline-success" type="submit">
                Search
              </Button>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default Header;
