import React from "react";
import { useLocation } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { bookmark } from "../redux/article/articleAction";
import { removeBookmark } from "../redux/article/articleAction";

const Details = () => {
  const location = useLocation();
  // console.log(location);
  const {
    state: { author, description, urlToImage, title, publishedAt },
  } = location;

  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(bookmark(location.state));
  };

  const bookMark = useSelector((state) => state.bookMark);

  console.log(bookMark, location.state);

  return (
    <div>
      <section className="light">
        <div className="container py-2">
          <div className="h1 text-center text-dark" id="pageHeaderTitle">
            Details
          </div>

          <article className="postcard light flex-column red">
            <div className="postcard__img_link">
              <img
                className="postcard__img img-fluid w-100"
                src={urlToImage}
                alt=" Title"
              />
            </div>
            <div className="postcard__text t-dark">
              <h1 className="postcard__title red">
                <a href="#!">{title}</a>
              </h1>
              <div className="postcard__subtitle small">
                <time>
                  <i className="fas fa-calendar-alt mr-2"></i>
                  {publishedAt.slice(0, 10)}
                </time>
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">{description}</div>
              <ul className="postcard__tagbox">
                <li className="tag__item">Author : {author}</li>
              </ul>
            </div>
          </article>
          <Link to="/">
            <Button variant="outline-primary" type="submit">
              Go Back
            </Button>
          </Link>

          {bookMark.find((item) => item.title === title) ? (
            <Button
              variant="outline-danger"
              type="submit"
              onClick={() => dispatch(removeBookmark(title))}
            >
              Remove Bookmark
            </Button>
          ) : (
            <Button
              variant="outline-success"
              type="submit"
              onClick={handleClick}
            >
              Bookmark
            </Button>
          )}
        </div>
      </section>
    </div>
  );
};

export default Details;
