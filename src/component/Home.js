import React from "react";
import { Table } from "react-bootstrap";
import Header from "./Header";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { loadMore } from "../redux/article/articleAction";
import { bookmark } from "../redux/article/articleAction";
import { removeBookmark } from "../redux/article/articleAction";

const Home = () => {
  const dispatch = useDispatch();

  const search = useSelector((state) => state.search);
  const news = useSelector((state) => state.news);

  const load = useSelector((state) => state.load);
  const bookMark = useSelector((state) => state.bookMark);

  // console.log(news, search, load);

  return (
    <div>
      <Header />

      {search ? (
        news.length === 0 ? (
          <div className="alert alert-danger" role="alert">
            No Results
          </div>
        ) : (
          <>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Published date</th>
                  <th>Bookmark</th>
                </tr>
              </thead>
              <tbody>
                {news.slice(0, load).map((item, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>
                      <Link to="/details" state={item}>
                        {item.title}
                      </Link>
                    </td>
                    <td>{item.author}</td>
                    <td>{item.publishedAt.slice(0, 10)}</td>
                    <td>
                      {bookMark.find((value) => value.title === item.title) ? (
                        <i
                          className="fas fa-star"
                          onClick={() => dispatch(removeBookmark(item.title))}
                          style={{
                            color: "cornflowerblue",
                          }}
                        ></i>
                      ) : (
                        <i
                          className="far fa-star"
                          onClick={() => dispatch(bookmark(item))}
                        ></i>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            {load < news.length && (
              <button
                type="button"
                className="btn btn-outline-primary"
                onClick={() => dispatch(loadMore())}
              >
                Load More
              </button>
            )}
          </>
        )
      ) : (
        <div className="alert alert-primary" role="alert">
          Search news!!
        </div>
      )}
    </div>
  );
};

export default Home;
