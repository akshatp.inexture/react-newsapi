import React from "react";
import { useSelector } from "react-redux";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Header from "./Header";

const BookMark = () => {
  const bookMark = useSelector((state) => state.bookMark);
  console.log(bookMark);

  return (
    <div>
      <Header />
      {bookMark.length !== 0 ? (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Author</th>
              <th>Published date</th>
            </tr>
          </thead>
          <tbody>
            {bookMark.map((item, i) => (
              <tr key={i}>
                <td>{i + 1}</td>
                <td>
                  <Link to="/details" state={item}>
                    {item.title}
                  </Link>
                </td>
                <td>{item.author}</td>
                <td>{item.publishedAt.slice(0, 10)}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      ) : (
        <div className="alert alert-warning" role="alert">
          You have not Bookmarked anything!!
        </div>
      )}
    </div>
  );
};

export default BookMark;
