import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import BookMark from "./component/BookMark";
import Details from "./component/Details";
import Home from "./component/Home";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="details" element={<Details />} />
        <Route path="bookmark" element={<BookMark />} />
        bookmark
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
